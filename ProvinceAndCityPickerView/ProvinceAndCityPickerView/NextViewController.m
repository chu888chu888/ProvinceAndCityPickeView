//
//  NextViewController.m
//  ProvinceAndCityPickerView
//
//  Created by Apple on 14/12/3.
//  Copyright (c) 2014年 Apple. All rights reserved.
//

#import "NextViewController.h"
#import "LoacationView.h"

@interface NextViewController ()

@property (strong, nonatomic) IBOutlet UITextField *showCityTF;
@property (strong, nonatomic) IBOutlet UIButton *showButton;



@property (nonatomic, strong) NSMutableDictionary * dict;

@property (nonatomic, strong) NSMutableArray * pickerArray; // 省
@property (nonatomic, strong) NSMutableArray * subPickerArray; // 市
@property (nonatomic, strong) NSMutableArray * thirdPickerArray; // 区
@property (nonatomic, strong) NSMutableArray * selectArray;

@property (nonatomic, strong) NSMutableArray * stringArray;

@property (nonatomic, strong) LoacationView * locationView;

@property (nonatomic, assign) NSInteger pickerIndex;
@property (nonatomic, assign) NSInteger subPickerIndex;
@property (nonatomic, assign) NSInteger thirdPickerIndex;

@end

@implementation NextViewController

#define kDurationTime 0.3
#define kPickerViewH 256

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString * plistPath = [[NSBundle mainBundle] pathForResource:@"Address" ofType:@"plist"];
    _dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    
    self.pickerArray = [[NSMutableArray alloc] initWithArray:[_dict allKeys]];
    self.selectArray = [[NSMutableArray alloc] initWithArray:[_dict objectForKey:[[_dict allKeys] objectAtIndex:0]]];
    if ([_selectArray count] > 0) {
        self.subPickerArray = [[NSMutableArray alloc] initWithArray:[[self.selectArray objectAtIndex:0] allKeys]];
    }
    
    if ([_subPickerArray count] > 0) {
        self.thirdPickerArray = [[NSMutableArray alloc] initWithArray:[[self.selectArray objectAtIndex:0] objectForKey:[self.subPickerArray objectAtIndex:0]]];
    }
    
    [self setupSubviews];
    
}

- (void)setupSubviews
{
    CGFloat locationViewY = 568 - kPickerViewH;
    _locationView = [[LoacationView alloc] initWithFrame:CGRectMake(0, locationViewY, self.view.bounds.size.width, kPickerViewH) andDataDic:_dict];
    _locationView.backgroundColor = [UIColor cyanColor];
    [_locationView haveSelectedCity:^(NSString *cityStr) {
        self.showCityTF.text = cityStr;
        [self touchesBegan:nil withEvent:nil];
        [self performSelectorInBackground:@selector(calculateLocationInArray) withObject:nil];

    }];
    
    [_locationView haveCancelSelect:^{
        [self touchesBegan:nil withEvent:nil];
        [self performSelectorInBackground:@selector(calculateLocationInArray) withObject:nil];
        
    }];
    [self.view addSubview:_locationView];
    
}

- (void)calculateLocationInArray
{
    // 计算出 当前显示的label上的 字的范围
    if (![self.showCityTF.text isEqual:[NSNull null]]) {
        NSString * cityString = self.showCityTF.text;
        NSArray * stringArray = [cityString componentsSeparatedByString:@","];
        self.stringArray = [[NSMutableArray alloc] initWithArray:stringArray];
        self.pickerIndex = [self getSelectIndexWithString:stringArray[0] array:self.pickerArray];
        [self reloadSubPickerArrayDataWithIndex:self.pickerIndex];
        
        self.subPickerIndex = [self getSelectIndexWithString:stringArray[1] array:self.subPickerArray];
        [self reloadThirdPickerArrayDataWithIndex:self.subPickerIndex];
        
        self.thirdPickerIndex = [self getSelectIndexWithString:stringArray[2] array:self.thirdPickerArray];
    }
}

- (void)reloadSubPickerArrayDataWithIndex:(NSInteger)num
{
    self.selectArray = [[NSMutableArray alloc] initWithArray:[_dict objectForKey:[[_dict allKeys] objectAtIndex:num]]];
    if ([_selectArray count] > 0) {
        self.subPickerArray = [[NSMutableArray alloc] initWithArray:[[self.selectArray objectAtIndex:0] allKeys]];
    }
}

- (void)reloadThirdPickerArrayDataWithIndex:(NSInteger)num2
{
    if ([_subPickerArray count] > 0) {
        self.thirdPickerArray = [[NSMutableArray alloc] initWithArray:[[self.selectArray objectAtIndex:0] objectForKey:[self.subPickerArray objectAtIndex:num2]]];
    }
}


- (NSInteger)getSelectIndexWithString:(NSString *)tempString array:(NSMutableArray *)array
{
    for (int i = 0; i < array.count; i++) {
        NSString * tpString = array[i];
        if ([tempString isEqualToString:tpString]) {
            return i;
        }
    }
    return 0;
}

#pragma mark - picker显示或隐藏
- (void)handlePickerShowOrNot:(UIButton *)button
{
    CGRect currentRect = self.locationView.frame;
    CGFloat pickerY = 0;
    if (currentRect.origin.y < self.view.bounds.size.height) {
        pickerY = self.view.bounds.size.height + kPickerViewH;
    } else if (currentRect.origin.y > self.view.bounds.size.height) {
        pickerY = self.view.bounds.size.height - kPickerViewH;
    }
    
    [UIView animateWithDuration:kDurationTime animations:^{
        self.locationView.frame = CGRectMake(currentRect.origin.x, pickerY, currentRect.size.width, currentRect.size.height);
        if (pickerY < self.view.bounds.size.height) {
            [self.locationView reloadLocationPickerViewWithPickerArray:self.pickerArray subPickerArray:self.subPickerArray thirdPickerArray:self.thirdPickerArray andStringArray:self.stringArray];
            [self.locationView.locationPickView selectRow:self.pickerIndex inComponent:0 animated:YES];
            [self.locationView.locationPickView selectRow:self.subPickerIndex inComponent:1 animated:YES];
            [self.locationView.locationPickView selectRow:self.thirdPickerIndex inComponent:2 animated:YES];
        }
    }];
}

- (IBAction)showLocationViewOrNot:(id)sender {
    [self handlePickerShowOrNot:sender];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGRect currentRect = self.locationView.frame;
    CGFloat currentY = self.view.bounds.size.height + kPickerViewH;
    
    [UIView animateWithDuration:kDurationTime animations:^{
        self.locationView.frame = CGRectMake(currentRect.origin.x, currentY, currentRect.size.width, currentRect.size.height);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)back:(id)sender {
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
