//
//  ViewController.m
//  ProvinceAndCityPickerView
//
//  Created by Apple on 14/12/3.
//  Copyright (c) 2014年 Apple. All rights reserved.
//

#import "ViewController.h"

#define FirstComponent 0
#define SubComponent 1
#define ThirdComponent 2
#define kDurationTime 0.3
#define kPickerViewH 180

@interface ViewController ()<UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) IBOutlet UIPickerView *cityPickerView;
@property (strong, nonatomic) IBOutlet UIButton *showPickerButton;
@property (strong, nonatomic) IBOutlet UITextField *cityTextField;


@property (nonatomic, strong) NSMutableDictionary * dict;
@property (nonatomic, strong) NSMutableArray * pickerArray; // 省
@property (nonatomic, strong) NSMutableArray * subPickerArray; // 市
@property (nonatomic, strong) NSMutableArray * thirdPickerArray; // 区
@property (nonatomic, strong) NSMutableArray * selectArray;

@property (nonatomic, strong) NSString * provinceString;
@property (nonatomic, strong) NSString * cityString;
@property (nonatomic, strong) NSString * areaString;

@end

@implementation ViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.provinceString = @"";
        self.cityString = @"";
        self.areaString = @"";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.provinceString = @"";
    self.cityString = @"";
    self.areaString = @"";
    
    [self setupSubviews];
    
    NSString * plistPath = [[NSBundle mainBundle] pathForResource:@"Address" ofType:@"plist"];
    _dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    self.pickerArray = [[NSMutableArray alloc] initWithArray:[_dict allKeys]];
    self.selectArray = [[NSMutableArray alloc] initWithArray:[_dict objectForKey:[[_dict allKeys] objectAtIndex:0]]];
    if ([_selectArray count] > 0) {
        self.subPickerArray = [[NSMutableArray alloc] initWithArray:[[self.selectArray objectAtIndex:0] allKeys]];
    }
    
    if ([_subPickerArray count] > 0) {
        self.thirdPickerArray = [[NSMutableArray alloc] initWithArray:[[self.selectArray objectAtIndex:0] objectForKey:[self.subPickerArray objectAtIndex:0]]];
    }
    
}

- (void)setupSubviews
{
    [self.showPickerButton addTarget:self action:@selector(handlePickerShowOrNot:) forControlEvents:UIControlEventTouchUpInside];
    
    self.cityPickerView.delegate = self;
    self.cityPickerView.dataSource = self;
    self.cityPickerView.backgroundColor = [UIColor lightGrayColor];
    
    [self.cityTextField setEnabled:NO];
}

#pragma mark - picker显示或隐藏
- (void)handlePickerShowOrNot:(UIButton *)button
{
    CGRect currentRect = self.cityPickerView.frame;
    CGFloat pickerY = 0;
    if (currentRect.origin.y < self.view.bounds.size.height) {
        pickerY = self.view.bounds.size.height + kPickerViewH;
    } else if (currentRect.origin.y > self.view.bounds.size.height) {
        pickerY = self.view.bounds.size.height - kPickerViewH;
    }
    
    [UIView animateWithDuration:kDurationTime animations:^{
        self.cityPickerView.frame = CGRectMake(currentRect.origin.x, pickerY, currentRect.size.width, currentRect.size.height);
    }];
}

#pragma mark - 
#pragma mark - UIPickerView DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case FirstComponent:
        {
            return [self.pickerArray count];
        }
            break;
        case SubComponent:
        {
            return [self.subPickerArray count];
        }
            break;
        case ThirdComponent:
        {
            return [self.thirdPickerArray count];
        }
            break;
            
        default:
            break;
    }
    return 0;
}

#pragma mark - 
#pragma mark - UIPickerView Delegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (component) {
        case FirstComponent:
        {
            return [self.pickerArray objectAtIndex:row];
        }
            break;
        case SubComponent:
        {
            return [self.subPickerArray objectAtIndex:row];
        }
            break;
        case ThirdComponent:
        {
            return [self.thirdPickerArray objectAtIndex:row];
        }
            break;
            
        default:
            break;
    }
    return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == FirstComponent) {
        self.provinceString = [self.pickerArray objectAtIndex:row];
        self.selectArray = [_dict objectForKey:[self.pickerArray objectAtIndex:row]];
        if (self.selectArray.count > 0) {
            self.subPickerArray = (NSMutableArray *)[[self.selectArray objectAtIndex:0] allKeys];
            
        } else {
            self.subPickerArray = nil;
        }
        
        if (self.subPickerArray.count > 0) {
            self.thirdPickerArray = [[self.selectArray objectAtIndex:0] objectForKey:[self.subPickerArray objectAtIndex:0]];
        } else {
            self.thirdPickerArray = nil;
        }
        
        
//        [pickerView selectedRowInComponent:1];
        [pickerView reloadComponent:1];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        
        self.cityString = [self.subPickerArray objectAtIndex:0];
        
        
//        [pickerView selectedRowInComponent:2];
        [pickerView reloadComponent:2];
        [pickerView selectRow:0 inComponent:2 animated:YES];
        
        self.areaString = [self.thirdPickerArray objectAtIndex:0];
        
    }
    
    if (component == SubComponent) {
        self.cityString = [self.subPickerArray objectAtIndex:row];
        
        if (self.selectArray.count > 0 && self.subPickerArray.count > 0) {
            self.thirdPickerArray = [[self.selectArray objectAtIndex:0] objectForKey:[self.subPickerArray objectAtIndex:row]];
        } else {
            self.thirdPickerArray = nil;
        }
        [pickerView selectRow:0 inComponent:2 animated:YES];
        [pickerView reloadComponent:2];
        
        self.areaString = [self.thirdPickerArray objectAtIndex:0];
    }
    
    if (component == ThirdComponent) {
        self.areaString = [self.thirdPickerArray objectAtIndex:row];
    }
    
    self.cityTextField.text = [NSString stringWithFormat:@"%@,%@,%@", self.provinceString, self.cityString, self.areaString];

}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (component == FirstComponent) {
        return 90.0;
    }
    
    if (component == SubComponent) {
        return 120.0;
    }
    
    if (component == ThirdComponent) {
        return 100.0;
    }
    return 0;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel * pickerLabel = (UILabel *)view;
    if (!pickerLabel) {
        pickerLabel = [[UILabel alloc] init];
        pickerLabel.adjustsFontSizeToFitWidth = YES;
        [pickerLabel setTextAlignment:NSTextAlignmentLeft];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont systemFontOfSize:17]];
    }
    pickerLabel.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGRect currentRect = self.cityPickerView.frame;
    CGFloat currentY = self.view.bounds.size.height + kPickerViewH;
    
    [UIView animateWithDuration:kDurationTime animations:^{
        self.cityPickerView.frame = CGRectMake(currentRect.origin.x, currentY, currentRect.size.width, currentRect.size.height);
    }];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
